/**
 *
 * @author 朱雀
 */
Ext.define('Skill.layout.Border', {
	extend: 'Ext.panel.Panel',
	layout: 'border',
	initComponent: function() {
		var me = this;
		me.initWestTree(me);
		me.items = [me.westTree];
		me.callParent();
	},
	initWestTree: function(me) {
		me.westTree = Ext.create('Skill.panel.SkillTree', {
			region: 'west',
			width: 200
		});
	},
	initCenterTabPanel: function(me) {

	}
});