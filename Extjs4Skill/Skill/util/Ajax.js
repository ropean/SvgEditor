/**
 *
 * @author 朱雀
 */
Ext.define('Skill.util.Ajax', {
	ajax: function(method, url, config) {
		var returnText = null;
		Ext.Ajax.request({
			url: url,
			async: false,
			method: method,
			params: config == null ? null : config.params,
			jsonData: config == null ? null : config.jsonData,
			form: config == null ? null : config.form,
			success: function(request) {
				returnText = request.responseText;
				if (returnText == null || returnText == "") {
					returnText = true;
				}
			},
			failure: function() {
				returnText = null;
			}
		});
		return returnText;
	}
});