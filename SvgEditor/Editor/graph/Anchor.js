/**
 *
 * @author 朱雀
 */
Ext.define('SvgEditor.graph.Anchor', {
	extend: 'SvgEditor.graph.Circle',
	onDDStart: function(x, y, e) {
		var anchor = this, canvas = anchor.canvas, model = canvas.selected[0],
				lineX = canvas.lineX, pathX = lineX.attrs.path,
				lineY = canvas.lineY, pathY = lineY.attrs.path;
		lineX.show();
		lineY.show();
		anchor.oldX = anchor.attrs.cx;
		anchor.oldY = anchor.attrs.cy;
		pathX[0][1] = pathX[1][1] = -10;
		lineX.attr({path: pathX});
		pathY[0][2] = pathY[1][2] = -10;
		lineY.attr({path: pathY});
		anchor.toBack();
		var flow = model, path = flow.attrs.path, flowFrom = flow.flowFrom, flowTo = flow.flowTo, start, end;
		if (anchor.id === 'startP' || anchor.id === 'endP' || anchor.index === 1 || anchor.index === path.length - 2) {
			if (flowFrom) {
				start = [flowFrom.coreX, flowFrom.coreY];
			} else {
				start = flow.start;
			}
			if (flowTo) {
				end = [flowTo.coreX, flowTo.coreY];
			} else {
				end = flow.end;
			}
			path[0] = ['M', start[0], start[1]];
			path[path.length - 1] = ['L', end[0], end[1]];
			flow.attr({path: path});
			flow.start = start;
			flow.end = end;
		}
	},
	onDDMove: function(dx, dy, x, y, e) {
		var anchor = this, canvas = anchor.canvas, flow = canvas.selected[0], path = flow.attrs.path,
				lineX = canvas.lineX, pathX = lineX.attrs.path,
				lineY = canvas.lineY, pathY = lineY.attrs.path,
				newX = anchor.oldX + dx, newY = anchor.oldY + dy;
		newX = Raphael.snapTo(5, newX);
		newY = Raphael.snapTo(5, newY);
		anchor.attr({cx: newX, cy: newY});
		anchor.coreX = anchor.attrs.cx;
		anchor.coreY = anchor.attrs.cy;
		pathX[0][1] = pathX[1][1] = anchor.attrs.cx;
		lineX.attr({path: pathX});
		pathY[0][2] = pathY[1][2] = anchor.attrs.cy;
		lineY.attr({path: pathY});
		if (anchor.id === 'startP') {
			path[0] = ['M', anchor.coreX, anchor.coreY];
			path[path.length - 1] = ['L', flow.end[0], flow.end[1]];
			flow.attr({path: path});
		} else if (anchor.id === 'endP') {
			path[0] = ['M', flow.start[0], flow.start[1]];
			path[path.length - 1] = ['L', anchor.coreX, anchor.coreY];
			flow.attr({path: path});
		} else {
			path[anchor.index] = ['L', anchor.coreX, anchor.coreY];
			flow.attr({path: path});
		}
	},
	onDDEnd: function(x, y, e) {
		var anchor = this, canvas = anchor.canvas, model = canvas.selected[0];
		if (model.type === 'path') {
			var flow = model, flowPlate = flow.flowPlate, path = flow.attrs.path;
			flowPlate.attr({path: path});
			flowPlate.start = flow.start = [path[0][1], path[0][2]];
			flowPlate.end = flow.end = [path[path.length - 1][1], path[path.length - 1][2]];
			if (anchor.id === 'startP') {
				canvas.connect(flow, canvas.target, null);
			} else if (anchor.id === 'endP') {
				canvas.connect(flow, null, canvas.target);
			} else if (anchor.index === 1 || anchor.index === path.length - 2) {
				canvas.connect(flow);
			}
			canvas.viewAnchor(canvas, flow);
		}
		canvas.lineX.hide();
		canvas.lineY.hide();
	}
});