Ext.define('SvgEditor.property.Property', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'group', // for grouping
			type: 'string'
		},
		{
			name: 'name',
			type: 'string'
		},
		{
			name: 'value'
		},
		{
			name: 'icons'
		},
		{
			name: 'gridProperties'
		}
	]
});