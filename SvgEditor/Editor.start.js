/**
 *
 * @author 朱雀
 */

Ext.Loader.setPath('SvgEditor', 'Editor');
Ext.require('SvgEditor.init.Editor');
Ext.onReady(function() {
	editor = Ext.create('SvgEditor.init.Editor', {readOnly: false});
	var init = Ext.create('SvgEditor.init.Init', {editor: editor});
	editor.ep.topBar.initUtil = init;
	init.init();
});